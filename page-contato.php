<?php
/**
* Template Name: Contato
*/
get_header(); ?>
	<?php include (TEMPLATEPATH . '/inc/area-logo.php'); ?>
		<section class="barra-meio">
			<div class="container">
			<div id="contato">
			<div class="row">
			<h2 class="contato">Contato</h2>
				<div class="col-md-6">
					<form action="" method="post" class="form">
						<div class="row">
						<div class="col-md-6">
				<input type="text" placeholder="Nome" class="form-control">
						</div>
						<div class="col-md-6">
				<input type="text" placeholder="Cidade/Estado" class="form-control">
						</div>
						<div class="col-md-12">
				<input type="text" placeholder="Assunto" class="form-control">
						</div>
						<div class="col-md-12">
				<textarea name="" placeholder="Mensagem" class="form-control"></textarea>
						</div>
				<button class="btn pull-right" type="submit">Enviar</button>
						</div>
					</form>

				</div><!-- fim col -->
			</div><!-- fim row -->
			</div><!-- fim /contato -->
			</div><!-- fim container meio -->
		</section>

<?php get_footer(); ?>