<?php
/**
* Template Name: Agenda
*/

$args = array (
'post_type'              => 'shows',
'posts_per_page'         => -1,
'order'                  => 'ASC',
'orderby'                => 'meta_value',
//'posts_per_page'		 => 3
);

$query = new WP_Query($args);
if($query->have_posts()):

get_header(); ?>
	<?php include (TEMPLATEPATH . '/inc/area-logo.php'); ?>
		<section class="barra-meio">
			<div class="container">
			<div id="contato">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<h2 class="titulo"><span>Próximos</span> Shows</h2>
				<div id="agenda">
					<div class="agenda-body">
					<div class="row">
					<?php while($query->have_posts()): $query->the_post(); ?>
					<?php $date = DateTime::createFromFormat('Ymd', get_field('data_do_evento')); ?>
					<article class="shows col-xs-12 col-sm-12 col-md-12 col-lg-12" id="show-<?php the_ID(); ?>">
						<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
							<div class="data"><?php echo is_object($date) ? $date->format('d') : ''; ?><span>/<?php echo is_object($date) ? $date->format('m') : ''; ?></span></div>
						</div><!-- col 5 -->
						<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
							<div class="cidade-local">
						<?php the_field('cidade'); ?>
						<div class="local">LOCAL: <span><?php the_field('local'); ?></span></div>
							</div><!-- /cidade-local -->
						</div><!-- col 7 -->
					</article>
				<?php endwhile; ?>
				</div><!-- fim row#2 -->
					</div><!-- fim agenda body -->
				</div><!-- fim agenda -->
				</div><!-- fim col -->
			</div><!-- fim row -->
			</div><!-- fim /contato -->
			</div><!-- fim container meio -->
		</section>
	<?php endif; ?>

<?php get_footer(); ?>