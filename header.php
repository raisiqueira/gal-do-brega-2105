<!DOCTYPE html>
<?php global $redux_demo; ?>
<html lang="pt-BR">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="theme-color" content="#9d7399">
		<title></title>

		<!-- Bootstrap CSS -->
		<link href="<?php bloginfo('template_directory'); ?>/assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
		<link href="<?php bloginfo('template_directory'); ?>/fonts/stylesheet.css" rel="stylesheet">
		<link href="<?php bloginfo('template_directory'); ?>/css/jquery.mCustomScrollbar.css" rel="stylesheet">
		<link href="<?php bloginfo('template_directory'); ?>/assets/flickity/dist/flickity.min.css" rel="stylesheet">
		<style type="text/css" media="screen">
		@import url( <?php bloginfo('stylesheet_url'); ?> );
</style>

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
<?php wp_head(); ?>
</head>
	<body>
	<!-- galã do brega -->
	<div class="site">
	<div id="gala">
	<div class="parallax-foto">
	<img src="<?php echo $redux_demo['upload-gala']['url'] ?>" alt="">
	</div><!-- parallax -->
	</div><!-- galã -->
	</div><!-- site -->
	<!-- inicio Header -->
		<header id="header" class="clearfix">
				<nav class="navbar navbar-custom" role="navigation">
					<div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    <i class="fa fa-bars"></i>
                </button>
            </div>
            <div class="collapse navbar-collapse navbar-left navbar-main-collapse">
					<ul class="nav navbar-nav">
						<li><a href="<?php echo get_settings('home'); ?>">Página Inicial</a></li>
						<li><a href="<?php echo get_settings('home'); ?>/agenda">Agenda</a></li>
						<li><a href="<?php echo get_settings('home'); ?>/fotos">Fotos</a></li>
						<li><a href="<?php echo get_settings('home'); ?>/musicas">Músicas</a></li>
						<li><a href="<?php echo get_settings('home'); ?>/videos">Vídeos</a></li>
						<li><a href="<?php echo get_settings('home'); ?>/contato">Contato</a></li>
					</ul>
				</div><!-- fim colapse -->
					<div class="social pull-right">
						<li><a href="http://instagram.com/<?php echo $redux_demo['social-instagram']; ?>" title="Instagram"><i class="fa fa-instagram"></i></a></li>
						<li><a href="http://facebook.com/<?php echo $redux_demo['social-fb']; ?>" title="Facebook"><i class="fa fa-facebook-square"></i></a></li>
						<li><a href="http://twitter.com/<?php echo $redux_demo['social-tw']; ?>" title="Twitter"><i class="fa fa-twitter-square"></i></a></li>
						<li><a href="http://youtube.com/<?php echo $redux_demo['social-yt']; ?>" title="Youtube"><i class="fa fa-youtube"></i></a></li>
					</div><!-- social menu -->
						</div><!-- fim social menu -->
				</nav>
		</header><!-- /header -->