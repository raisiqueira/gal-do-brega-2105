<?php get_header(); ?>
	<?php include (TEMPLATEPATH . '/inc/area-logo.php'); ?>

		<section class="barra-meio">
			<div class="container">
			<div id="video">

			<div class="row">
			<?php if (have_posts()): while (have_posts()) : the_post();?>
				<div class="col-md-6">
			<h2 class="titulo"><?php the_title(); ?></h2>
					<div class="row">
						<div id="contato" class="col-md-12">
						<?php the_content(); ?>
						</div>
					</div><!-- /.row colunas internas -->
				</div><!-- fim div coluna -->
			<?php endwhile;?>
				<?php endif; ?>
			</div><!-- fim /.row -->
			</div><!-- fim /#fotos -->
			</div><!-- fim container meio -->
		</section>

<?php get_footer(); ?>