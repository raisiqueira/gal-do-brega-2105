		<?php global $redux_demo; ?>
		<div class="container">
		<section class="contatos">
		<div class="row">
			<hr class="divisor">
			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
				<div class="item">
				<i class="fa fa-download icone"></i><a href="<?php echo $redux_demo['url-divulgacao']; ?>" title="Material de Divulgação">Material de Divulgação</a>
				</div><!-- fim item -->
			</div>

			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
				<div class="item">
				<i class="fa fa-phone icone"></i><?php echo $redux_demo['telefone-rodape']; ?>
				</div><!-- fim item -->
			</div>

			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
				<div class="item">
					<i class="fa fa-envelope icone"></i><a href="mailto:<?php echo $redux_demo['email-rodape']; ?>" title="Contato"><?php echo $redux_demo['email-rodape']; ?></a>
				</div><!-- fim item -->
			</div>
		</div><!-- fim row -->
		</section>
		</div><!-- fim container -->

		<footer id="rodape">
			<div class="container">
				<div class="row">
					<div class="col-xs-5 col-sm-5 col-md-6 col-lg-6">
						<div class="pull-left copy">
						Galã do Brega - © <?php echo date('Y') ?> Todos os direitos reservados.
						</div>
					</div><!-- /col 6#1 -->

					<div class="col-xs-7 col-sm-7 col-md-6 col-lg-6">
					<div class="pull-right">
						<div class="creditos">
							<a href="http://baixeshows.com.br" title="Studio Koalla -Rai Siqueira" class="koalla"></a>
						</div>
					</div>
					</div><!-- /col 6#2 -->
				</div><!-- fim div row -->
			</div><!-- fim div container rodape -->
		</footer>
		<!-- jQuery -->
		<script src="<?php bloginfo('template_directory'); ?>/assets/jquery/dist/jquery.min.js"></script>
		<!-- Bootstrap JavaScript -->
		<script src="<?php bloginfo('template_directory'); ?>/assets/bootstrap/dist/js/bootstrap.min.js"></script>
		<script src="<?php bloginfo('template_directory'); ?>/js/jquery.mCustomScrollbar.concat.min.js"></script>
		<script src="<?php bloginfo('template_directory'); ?>/assets/flickity/dist/flickity.pkgd.min.js"></script>
		<script src="<?php bloginfo('template_directory'); ?>/js/plax.js"></script>
		<script type="text/javascript">
      $(document).ready(function () {
        $('.parallax-foto').plaxify({"xRange":50,"yRange":70})
        $.plax.enable()
      })
    </script>

	<script>
		(function($){
			$(window).load(function(){
				$("#agenda .agenda-body").mCustomScrollbar({
					setHeight:400,
					theme:"inset-2-light"
				});
			});
		})(jQuery);
	</script>
	<?php wp_footer(); ?>
	</body>
</html>