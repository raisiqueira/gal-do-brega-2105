<?php
/**
* Template Name: Fotos
*/
$args = array (
'post_type'              => 'fancy-gallery',
'posts_per_page'         => 4,
'order'                  => 'ASC',
'orderby'                => 'meta_value',
//'posts_per_page'		 => 3
);

get_header();
$query = new WP_Query($args);
if($query->have_posts()):
?>
	<?php include (TEMPLATEPATH . '/inc/area-logo.php'); ?>

		<section class="barra-meio">
			<div class="container">
			<div id="fotos">

			<div class="row">
				<div class="col-md-6">
				<h2 class="titulo">Fotos <a href="#myModal" role="button" class="btn btn-large btn-primary" data-toggle="modal"><i class="fa fa-instagram"></i> Instagram</a></h2>
					<div class="row">
<!-- modal instagram -->

<!-- Modal HTML -->
<div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" style="color:#000">Galã do Brega no Instagram</h4>
            </div>
            <div class="modal-body">
                <?php echo do_shortcode('[instagram-feed]') ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<!-- fim modal -->
					<?php while($query->have_posts()): $query->the_post(); ?>
						<div id="galeria-<?php the_ID(); ?>" class="col-md-6">
						<article class="foto">
							<?php if(has_post_thumbnail()): ?>
								<a href="<?php the_Permalink()?>" title="<?php the_title();?>"><?php the_post_thumbnail('galeria-fotos'); ?></a>
								<a href="<?php the_permalink(); ?>" class="mascara">
									<i class="fa fa-search-plus iczoom"></i>
									<h2><?php the_title();?></h2>
								</a><!-- fim mascara -->
							<?php else: ?>
								<a href="<?php the_Permalink()?>" title="<?php the_title();?>"><img src="<?php echo get_template_directory_uri(); ?>/imgs/fotos.png" alt=""></a>
								<a href="<?php the_permalink(); ?>" class="mascara">
									<i class="fa fa-search-plus iczoom"></i>
									<h2><?php the_title();?></h2>
								</a><!-- fim mascara -->
							<?php endif;?>
						</article>
						</div>
					<?php endwhile; ?>
					</div><!-- /.row colunas internas -->
				</div><!-- fim div coluna -->
			</div><!-- fim /.row -->
			</div><!-- fim /#fotos -->
			</div><!-- fim container meio -->
		<?php endif; ?>
		</section>

<?php get_footer(); ?>