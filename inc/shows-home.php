			<?php
				$args = array (
				'post_type'              => 'shows',
				'posts_per_page'         => -1,
				'order'                  => 'ASC',
				'orderby'                => 'meta_value',
				'posts_per_page'		 => 3
				);

	        $query = new WP_Query($args);
	        if($query->have_posts()):
			?>
			<div class="container">
			<div class="row">
				<h2 class="titulo-shows">Próximos <span>Shows</span></h2>
				<?php while($query->have_posts()): $query->the_post(); ?>
				<article class="data-show" id="show-<?php the_ID(); ?>">
					<div class="col-xs-12 col-sm-4 col-md-3 col-lg-2">
					<?php $date = DateTime::createFromFormat('Ymd', get_field('data_do_evento')); ?>
					<span class="data_um"><?php echo is_object($date) ? $date->format('d') : ''; ?></span><span class="data_dois">/<?php echo is_object($date) ? $date->format('m') : ''; ?></span>
					<h3 class="titulo"><?php the_field('cidade'); ?></h3>
					</div>
				</article>
				<?php endwhile; ?>
			</div><!-- fim div row -->
			</div><!-- fim container datas -->
			<?php endif;
			wp_reset_postdata();
			?>