<?php
/**
* Single: Galleria Fancy Gallery
*/
get_header();?>
	<?php include (TEMPLATEPATH . '/inc/area-logo.php'); ?>

		<section class="barra-meio">
			<div class="container">
			<div id="fotos">

			<div class="row">
				<div class="col-md-6">
			<h2 class="titulo">Fotos - <?php the_title(); ?></h2>
					<div class="row">
					<?php while ( have_posts() ) : the_post(); ?>
						<?php
						$id_cpt = $post->ID;
						?>
						<div id="galeria-<?php the_ID(); ?>" class="col-md-6">
						<?php echo do_shortcode('[gallery id="{$id_cpt}"]'); ?>
						</div>
					<?php endwhile; wp_reset_postdata(); ?>
					</div><!-- /.row colunas internas -->
				</div><!-- fim div coluna -->
			</div><!-- fim /.row -->
			</div><!-- fim /#fotos -->
			</div><!-- fim container meio -->
		</section>

<?php get_footer(); ?>