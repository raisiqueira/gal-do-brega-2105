<?php

//Painel de Ajustes

if ( !class_exists( 'ReduxFramework' ) && file_exists( dirname( __FILE__ ) . '/redux-framework/ReduxCore/framework.php' ) ) {
    require_once( dirname( __FILE__ ) . '/redux-framework/ReduxCore/framework.php' );
}
if ( !isset( $redux_demo ) && file_exists( dirname( __FILE__ ) . '/redux-framework/sample/sample-config.php' ) ) {
    require_once( dirname( __FILE__ ) . '/redux-framework/sample/sample-config.php' );
}


define('ZD_ROOT', dirname(realpath(__FILE__)).'/');
define('ZD_URL', get_stylesheet_directory_uri().'/');
define('ZD_IMAGE_URL', ZD_URL.'imgs/');

//==========================================================================
//   Registra post type
// =====================================================================
function video_post_type(){
    $labels = array(
        'name'               => 'Vídeos',
        'singular_name'      => 'Vídeo',
        'add_new'            => 'Adicionar Novo',
        'add_new_item'       => 'Adicionar Novo Vídeo',
        'edit_item'          => 'Editar Vídeo',
        'new_item'           => 'Novo Vídeo',
        'all_items'          => 'Todas os Vídeo',
        'view_item'          => 'Ver Vídeos',
        'search_items'       => 'Buscar Vídeos',
        'not_found'          => 'Nenhum Vídeo encontrada',
        'not_found_in_trash' => 'Nenhum Vídeo na lixeira',
        'parent_item_colon'  => '',
        'menu_name'          => 'Vídeos'
        );

    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array('slug' => 'video'),
        'capability_type'    => 'post',
        'has_archive'        => 'video',
        'hierarchical'       => false,
        'menu_position'      => 5,
        'supports'           => array('title', 'editor',  'thumbnail'),
        'taxonomies'     => array(),
        );

    register_post_type('video', $args);
}

add_action('init', 'video_post_type');

function shows_posts_types() {

    $labels = array(
        'name'                => _x( 'Shows', 'Post Type General Name', 'bs' ),
        'singular_name'       => _x( 'Shows', 'Post Type Singular Name', 'bs' ),
        'menu_name'           => __( 'Shows', 'bs' ),
        'name_admin_bar'      => __( 'Shows', 'bs' ),
        'parent_item_colon'   => __( 'Parent Item:', 'bs' ),
        'all_items'           => __( 'Todos os Shows', 'bs' ),
        'add_new_item'        => __( 'Add novo Show', 'bs' ),
        'add_new'             => __( 'Novo Show', 'bs' ),
        'new_item'            => __( 'Novo Item', 'bs' ),
        'edit_item'           => __( 'Editar Item', 'bs' ),
        'update_item'         => __( 'Atualizar Item', 'bs' ),
        'view_item'           => __( 'Ver Item', 'bs' ),
        'search_items'        => __( 'Buscar', 'bs' ),
        'not_found'           => __( 'Não encontrado', 'bs' ),
        'not_found_in_trash'  => __( 'Nada encontrado no lixo', 'bs' ),
    );
/*    $args = array(
        'label'               => __( 'shows', 'bs' ),
        'description'         => __( 'Agenda de Shows', 'bs' ),
        'labels'              => $labels,
        'supports'            => array( 'title'),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'menu_position'       => 5,
        'show_in_admin_bar'   => true,
        'show_in_nav_menus'   => true,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
    ); */

        $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array('slug' => 'show'),
        'menu_icon'           => 'dashicons-format-audio',
        'capability_type'    => 'post',
        'has_archive'        => 'show',
        'hierarchical'       => false,
        'menu_position'      => 3,
        'supports'           => array('title'),
        'taxonomies'     => array(),
        );
    register_post_type( 'shows', $args );

}

// Hook into the 'init' action
add_action( 'init', 'shows_posts_types');


//==========================================================================
//   WP Thumbs
// =====================================================================
if ( function_exists( 'add_theme_support')){
    add_theme_support( 'post-thumbnails' );
}
add_image_size( 'galeria-fotos', 247, 247, true); //galeria thumbnail