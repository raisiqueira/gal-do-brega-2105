<?php
/**
* Template Name: Videos
*/
get_header();
$args = array (
            'post_type' => 'video',
            'posts_per_page' => -1,
);

$query = new WP_Query($args);
if($query->have_posts()):

?>
	<?php include (TEMPLATEPATH . '/inc/area-logo.php'); ?>

		<section class="barra-meio">
			<div class="container">
			<div id="video">

			<div class="row">
				<div class="col-md-6">
			<h2 class="titulo">Videos</h2>
					<div class="row">
						<div id="video-<?php get_the_id(); ?>" class="col-md-12 gallery js-flickity" data-flickity-options='{ "asNavFor": ".gallery", "contain": true, "prevNextButtons": false, "pageDots": false }'>
					<?php $i = 0; while($query->have_posts()): $query->the_post(); ?>
					<?php if($i > 2) continue; ?>
						<?php
		                preg_match_all('#[-a-zA-Z0-9@:%_\+.~\#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~\#?&//=]*)?#si', get_the_content(), $urls);
		                $video_url = $urls[0][0];
		                parse_str(parse_url($video_url, PHP_URL_QUERY ), $url_vars);
		                $video_id = $url_vars['v'];
		                $video_thumb_url = $video_id ? 'http://i1.ytimg.com/vi/'.$video_id.'/mqdefault.jpg' : null;

		                $post_thumbnail_url = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_id()), 'post-thumb');
		                $post_thumbnail_url = $post_thumbnail_url[0] ? $post_thumbnail_url[0] : $video_thumb_url;
		                $post_thumbnail_url = $post_thumbnail_url ? $post_thumbnail_url : ZD_IMAGE_URL.'default.jpg';
		                ?>

						<article class="video gallery-cell">
						<div class="item <?php echo $i == 0 ? 'active' : ''; ?>" id="vid-<?php the_id(); ?>">
                    <iframe class="molecule video-a" type="text/html" width="100%" height="350"
                    src="http://www.youtube.com/embed/<?php echo $video_id; ?>"
                    frameborder="0"></iframe>
                    <div class="video-titulo"><?php the_title(); ?></div>
                		</div>
						</article>
					<?php $i++; endwhile; ?>
					<?php rewind_posts(); ?>
						</div>
					</div><!-- /.row colunas internas -->
				</div><!-- fim div coluna -->
			</div><!-- fim /.row -->
			</div><!-- fim /#fotos -->
			</div><!-- fim container meio -->
		<?php endif; ?>
		</section>

<?php get_footer(); ?>